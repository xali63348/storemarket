<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href=" {{ URL::asset('show.css') }}">
    
   
</head>
<body>
@include('shared.navbar')
<section>
    
    <div class="w-50 p-1 m-auto">@include('shared.message')</div>
    <div id="content-wrapper">
            <div class="column">
            @isset($product->images)
                <img id=featured src="{{asset($product->images[0])}}">
            @endisset
            @empty($product->images)
                <img id=featured src="/images/product.jpeg">
            @endempty
                <div id="slide-wrapper" >
                    <img id="slideLeft" class="arrow" src="/images/arrow-left.png">

                    <div id="slider">
                        @isset($product->images)
                            @foreach($product->images as $image )
                                <img class="thumbnail active" src="{{asset($image)}}">
                            @endforeach
                        @endisset
                        @empty($product->images)
                            <img class="thumbnail active" src="/images/product.jpeg">
                        @endempty
                    </div>
                    <img id="slideRight" class="arrow" src="/images/arrow-right.png">
                </div>
            </div>

            <div class="column py-5 text-center">
                <h1>{{$product->name}}</h1>
                
                <h4>{{$product->price}}$</h4>

                <p>{{$product->description}}</p>

                <form method="post" action="/orders/addProduct/{{$product->id}}">
                @csrf  
                <label for=""><h4>{{__('messages.Quantity')}}</h4></label>
                <select name="quantity" class="form-select w-25 m-auto" aria-label="Default select example">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
                <br>
                <button type="submit" class="btn btn-dark">{{__('messages.Order')}}</button>
                </form>
        </div>
    </div>
    @include('layouts.footer')
</section>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

	<script type="text/javascript">
		let thumbnails = document.getElementsByClassName('thumbnail')

		let activeImages = document.getElementsByClassName('active')

		for (var i=0; i < thumbnails.length; i++){

			thumbnails[i].addEventListener('mouseover', function(){
				console.log(activeImages)

				if (activeImages.length > 0){
					activeImages[0].classList.remove('active')
				}


				this.classList.add('active')
				document.getElementById('featured').src = this.src
			})
		}


		let buttonRight = document.getElementById('slideRight');
		let buttonLeft = document.getElementById('slideLeft');

		buttonLeft.addEventListener('click', function(){
			document.getElementById('slider').scrollLeft -= 180
		})

		buttonRight.addEventListener('click', function(){
			document.getElementById('slider').scrollLeft += 180
		})
	</script>

    
</body>
</html>