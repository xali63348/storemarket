<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href=" {{ URL::asset('app.css') }}">
    
    @section('styles')
    @show
</head>
<body>
    @include('shared.navbar')
    @yield('contant')
   
<br>
<br>
<br>

@include('layouts.footer')
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <scrip src="/js/bootstrap.bundle.min.js"></script>
    @section('scrip')
    @show
</body>
</html>