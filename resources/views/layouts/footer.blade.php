<style>
    .boo{
   text-decoration: none;
   color: black;
}
.boo:hover{
    text-decoration: underline;
    color: blue;
}
.dropdown {
    position: relative;
    display: inline-block;
  }
  
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
  }
  
  .dropdown-content a {
    color: black;
    padding: 7px 10px;
    text-decoration: none;
    display: block;
    
  }
  .dropbtn {
    background-color: #4345da;
    color: white;
    padding: 10px;
    font-size: 16px;
    border: none;
    border-radius: 10px;
  }
  
  .dropdown-content a:hover {background-color: #ddd;}
  
  .dropdown:hover .dropdown-content {display: block;}
  
  .dropdown:hover .dropbtn {background-color: #2c2222;}
 

</style>

<footer class="bd-footer py-5 mt-5 bg-light">
  <div class="container py-5">
    <div class="row">
      <div class="col-lg-3 mb-3">
        <a class="d-inline-flex align-items-center mb-2 link-dark text-decoration-none" href="/" aria-label="Bootstrap">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="32" class="d-block me-2" viewBox="0 0 118 94" role="img"><title>Bootstrap</title><path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z" fill="currentColor"></path></svg>
          <span class="fs-5">{{__('messages.Budapest')}}</span>
        </a>
        <ul class="list-unstyled small text-muted">
          <li class="mb-2">{{__('messages.designed')}} <a href="https://www.instagram.com/700yz/" class="boo">{{__('messages.Ali Hussein')}}</a> {{__('messages.with the help of')}} <a href="https://www.barmej.com/"class="boo">{{__('messages.barmej')}}</a>.</li>
        </ul>
      </div>
      <div class="col-6 col-lg-2 offset-lg-1 mb-3">
        <h5>{{__('messages.Links')}}</h5>
        <ul class="list-unstyled">
          <li class="mb-2"><a href="/"class="boo">{{__('messages.home')}}</a></li>
          <li class="mb-2"><a href="/users/account"class="boo">{{__('messages.account')}}</a></li>
          <li class="mb-2"><a href="/stores/create"class="boo">{{__('messages.Create store')}}</a></li>
        </ul>
      </div>
      <div class="col-6 col-lg-2 mb-3">
        <h5>{{__("messages.Orders")}}</h5>
        <ul class="list-unstyled">
          <li class="mb-2"><a href="/stores/orders"class="boo">{{__('messages.Store Orders')}}</a></li>
          <li class="mb-2"><a href="/users/address"class="boo">{{__('messages.address')}}</a></li>
        </ul>
      </div>
     
      <div class="col-6 col-lg-2 mb-3">
      <div class="dropdown">
          <span><button class="dropbtn">{{__('messages.Language')}}<img src="{{ asset('/images/lang.png') }}" alt="Error"></button></span>
        <div class="dropdown-content">
          <a href="/toEnglish">English</a>
          <a href="/toArabic">العربية</a>
          
        </div>
      </div>
        
      </div>
    </div>
  </div>
</footer>