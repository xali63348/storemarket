<style>
 .card img{
      height:250px;
      object-fit:cover;
}
.card{
    min-width: 250px;
    max-width: 250px;
     
}
.img-card{
	min-width: 300px;
    max-width: 300px;
}
.col-lg-4{
	text-align:center;
}
.dd:hover{
	box-shadow:3px 3px 14px 15px #ddd ;
}

 p{
  font-size: 12px;
}
  .aa{
    text-decoration: none;
    color:#fff;
}
  .aa:hover{
    text-decoration: underline;
    color:#fff;
} 
</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    
    <style>
        @import url("https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css");
    </style>
</head>
<body>
@include('shared.navbar')
<section class="py-3">
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12">
			<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
				<div class="carousel-indicators">
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
				</div>
				<div class="carousel-inner">
					<div class="carousel-item active">
					<img src="/images/shop.png" class="d-block w-100" width="800" height="400"alt="...">
					</div>
					<div class="carousel-item">
					<img src="/images/shop2.jpg" class="d-block w-100" width="800" height="400" alt="...">
					</div>
					<div class="carousel-item">
					<img src="/images/shop3.png" class="d-block w-100" width="800" height="400"alt="...">
					</div>
				</div>
				<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button>
				</div>
			</div>
		</div>

	</div>
</section>
<section class="py-0">
    <div class="container ">
        <div class="row text-center">
        @foreach($product as $product)
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                  <div class="card bg-dark text-light mb-3 dd  ">
                      @isset($product->images[0])
                          <a href="/products/{{$product->id}}"><img src="{{asset($product->images[0])}}" alt="" class="card-img-top"></a>
                      @endisset
                      @empty($product->images[0])
                          <a href="/products/{{$product->id}}"><img src="{{asset('/images/product.jpeg')}}" class="card-img-top" alt="..."></a>
                      @endempty
                          <div class="card-body text-center">
                              <h4 class="card-title">{{ Illuminate\Support\Str::limit($product->name, 12,'...')}}</h4>
                              <a href="/products/{{$product->id}}"class="aa"><p class="card-text">{{ Illuminate\Support\Str::limit($product->description, 50, '...') }}</p></a>
                        </div>
                  </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@include('layouts.footer')
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

</body>
</html>





