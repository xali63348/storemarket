@extends('layouts.app')

@section('contant')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>{{$message}}</h3>
                <br/>
                <a href="/users/account" class="btn btn-outline-primary">{{__('messages.Back to the stores')}}</a>
            </div>
        </div>
    </div>
    
@endsection