@extends('layouts.app')

@section('contant')

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>{{$message}}</h3>
                <br/>
                <a href="/stores/orders" class="btn btn-outline-primary">{{__('messages.Back to the store order')}}</a>
            </div>
        </div>
    </div>
    
@endsection