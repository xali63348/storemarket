@component('mail::message')
Hi {{$user->name}}

Please enter your full address to be able to buy from the site

@component('mail::button', ['url' => 'http://localhost:8000/users/address'])
add address
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
