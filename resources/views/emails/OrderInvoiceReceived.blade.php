@component('mail::message')
Hi {{$order->user->name}}

Thank you for shopping from our store. Please go to the link to see the status of your order and all the details.

@component('mail::button', ['url' => 'http://localhost:8000/orders/order'])
orders
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
