@component('mail::message')
You have a new order.

Order Number: {{$order->transaction_id}}

Payment: {{$order->payment_type}}

Store: {{$order->store->name}}

@component('mail::button', ['url' => 'http://localhost:8000/stores/orders'])
Store Order
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
