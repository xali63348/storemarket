@component('mail::message')
Hi {{$user->name}}

Welcome to Zulekha store You can now shop and see the best products

@component('mail::button', ['url' => 'http://localhost:8000/'])
Vist
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
