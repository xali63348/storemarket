@extends('layouts.app')

@section('styles')
    <style>
        table{
            text-align: center;
        }
        .id1{
            margin-right:20%;
           width: 20px;
        }
        li {
            list-style: none;
        }
        .back{
            text-decoration:none;
        }
        .back:hover{
            text-decoration:underline;
            color:black;

        }
    </style>

@endsection


@section('contant')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <a href="/orders/order" class="back">{{__('messages.Back to Talabat')}}</a> 
        </div>
        <br>
        <br>
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 py-4">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">{{__('messages.Id')}}</th>
                <th scope="col">{{__('messages.Name product')}}</th>
                <th scope="col">{{__('messages.Unit Price')}}</th>
                <th scope="col">{{__('messages.Quantity')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->products as $prodect)
                <tr>
                <th scope="row">{{$prodect["id"]}}</th>
                <td>{{$prodect["name"]}}</td>
                <td>{{$prodect["price"]}}$</td>
                <td>{{$prodect["quantity"]}}</td>
                </tr>
                @endforeach
                
            </tbody>
            </table>
            <br>
      
            <h5>Total Price: {{$total}}$</h5>
        </div>
        <div class="col-12 col-sm-12 col-md-8 col-lg-4">
            <div class="card text-white bg-primary mb-3" style="max-width: 40rem;">
            <div class="card-header">{{__('messages.customer information')}}</div>
            <div class="card-body">
                <h6 class="card-title">{{__('messages.name')}}: {{$order->user->name}}</h6>
            <p class="card-text">
            {{__('messages.number phone')}}: {{$order->user->number}} <br>
            {{__('messages.email')}}: {{$order->user->email}} <br>
            {{__('messages.address')}}:
                <li> {{__('messages.country')}}: {{$order->user->address['Country']}}</li>
                <li> {{__('messages.area')}}: {{$order->user->address['area']}}</li>
                <li> {{__('messages.block')}}: {{$order->user->address['block']}}</li>
                <li> {{__('messages.street')}}: {{$order->user->address['street']}}</li>
                <li> {{__('messages.house')}}: {{$order->user->address['house']}}</li>
                <li> {{__('messages.additional information')}}: {{$order->user->address['extra']}}</li>
            </p>
            </div>
        </div>
        </div>
       
    </div>
</div>
@endsection