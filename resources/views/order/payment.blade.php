@extends('layouts.app')

@section('contant')
<div class="container">
    <div class="row">
        <div class="col-12-lg text-center">
            <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">{{__('messages.Well done!')}}</h4>
            <p>{{__('messages.The process was completed successfully,please see the requests page')}} <a href="/orders/order">{{__('messages.here')}}</a></p>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
@endsection