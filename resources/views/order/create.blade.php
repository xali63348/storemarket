@extends('layouts.app')

@section('styles')
<style>
.btn-outline-primary{
     margin-left:13px;
}
.pa{
    margin-top:8px;
}
.btn-danger{
    margin-left:13px;
}

</style>
    
@endsection
@section('contant')
<div class="container">
    <div class="row text-center">
        <div class="table-responsive col-lg-12 ">
        <table class="table">
            <thead>
                <tr>
                <th scope="col">{{__('messages.Id')}}</th>
                <th scope="col">{{__('messages.Name product')}}</th>
                <th scope="col">{{__('messages.Unit Price')}}</th>
                <th scope="col">{{__('messages.Quantity')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach(session('carruntOreder') as $product)
                <tr>
                    <th scope="row">{{$product->id}}</th>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}$</td>
                    <td>{{$product->quantity}}</td>
                </tr>
                @endforeach
               
            </tbody>
            </table>
              <span><h5>{{__('messages.Total Price')}}: {{$total}}$</h5></span>
              <br>
              @if(empty(auth()->user()->address))
              <span><a href="/orders/Payment" class="btn btn-outline-dark disabled">{{__('messages.Cash')}}</a>
                <a href="/" class="btn btn-outline-primary">{{__('messages.Continue shopping')}}</a>
                <a href="/orders/delete" class="btn btn-danger">{{__('messages.Delete Basket')}}</a>
              </span>
              <p class="pa">{{__('messages.Please enter your address')}} <a href="/users/address">{{__('messages.here')}}</a></p>
              @else

              <!-- <span><a href="/orders/payment" class="btn btn-outline-dark">{{__('messages.Cash')}}</a>
              <span><a href="/orders/buy" class="btn btn-outline-dark">buy</a> -->
 
              <a href="/" class="btn btn btn-outline-primary ">{{__('messages.Shop more')}}</a>
              <a href="/orders/delete" class="btn btn-danger">{{__('messages.Delete Basket')}}</a>
              </span>
              <form class="py-3"action="/create-payment" method="post">
                @csrf
                    <input type="submit" class="btn btn-primary" value="paypal">
            </form>
            @endif
            

          
        </div>
    </div>
</div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
@endsection


