@extends('layouts.app')

@section('contant')
<section>
    <div class="container">
        <div class="row text-center">
        <div class="col-12 col-sm-12 col-lg-3">
                @component('shared.menu',['orders'=>'active'])
                @endcomponent
        </div>
            <div class="table-responsive col-lg-9 ">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">{{__('messages.Id')}}</th>
                    <th scope="col">{{__('messages.Stores')}}</th>
                    <th scope="col">{{__('messages.Order number')}}</th>
                    <th scope="col">{{__('messages.Payment')}}</th>
                    <th scope="col">{{__('messages.Status')}}</th>
                    <th scope="col">{{__('messages.Product')}}</th>
                    <th scope="col">{{__('messages.Procedures')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                    <th scope="row text-center">{{$order->id}}</th>
                    <td>{{$order->store->name}}</td>
                    <td>{{$order->transaction_id}}</td>
                    <td>{{$order->payment_type}}</td>
                    <td>{{$order->status}}</td>
                    <td>{{sizeof($order->products)}}</td>
                    <td>
                        <a href="/orders/{{$order->id}}" class="btn btn-outline-dark">{{__('messages.details')}}</a>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            
        </div>
    </div>
</section>
@endsection