<div class="list-group">
    <a href="/users/account" class="list-group-item list-group-item-action {{$store ?? ''}}" aria-current="true">
        {{__('messages.Stores')}}
    </a>
    <a href="/orders/order" class="list-group-item list-group-item-action {{$orders ?? ''}}">{{__('messages.Orders')}}</a>
    <a href="/stores/create" class="list-group-item list-group-item-action {{$create ?? ''}}">{{__('messages.Create store')}}</a>
    @if(sizeof(auth()->user()->store)>0)
    <a href="/stores/orders" class="list-group-item list-group-item-action {{$StoreOrder ?? ''}}">{{__('messages.Store Orders')}}</a>
    @endif
    <a href="/users/address" class="list-group-item list-group-item-action {{$address ?? ''}}">{{__('messages.address')}}</a>
    <a href="/users/edit" class="list-group-item list-group-item-action {{$information ?? ''}}">{{__('messages.Account Information')}}</a>
</div>
