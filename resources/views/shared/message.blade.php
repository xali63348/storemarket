


@if(session()->has('message'))
<div class="alert alert-success" role="alert">
@foreach (session('message') as $message)
    <li class="lii">{{$message}}</li>
@endforeach
</div>

@endif