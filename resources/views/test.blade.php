<style>
.card img{
      height:250px;
      object-fit:cover;
    }
    .card{
      min-width: 300px;
      max-width: 300px;
     
    }
	.img-card{
	  min-width: 300px;
      max-width: 300px;
	}
	.col-lg-4{
		text-align:center;
	}
	.dd:hover{
		box-shadow:2px 2px 14px 10px #ddd ;
	}

</style>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <section>
<nav>
	<input id="nav-toggle" type="checkbox">
	<div class="logo">Zulekha</div>
	<ul class="links">
		<li><a href="/">Home</a></li>
		<li><a href="/users/account">Account</a></li>
		<li><a href="#work">About</a></li>
		<li><a href="#projects">Support</a></li>
         <!-- <a href="/orders/create"> basket<i class="fas fa-shopping-cart"></i> </a> -->
		<li><a href="/users/login"class="btn btn-outline-dark">Sing in</a></li>
		
	</ul>
	<label for="nav-toggle" class="icon-burger">
		<div class="line"></div>
		<div class="line"></div>
		<div class="line"></div>
	</label>
</nav>
</section>
<section class="py-5">
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-12">
			<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
				<div class="carousel-indicators">
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
					<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
				</div>
				<div class="carousel-inner">
					<div class="carousel-item active">
					<img src="/images/shop.png" class="d-block w-100" width="800" height="400"alt="...">
					</div>
					<div class="carousel-item">
					<img src="/images/shop2.jpg" class="d-block w-100" width="800" height="400" alt="...">
					</div>
					<div class="carousel-item">
					<img src="/images/shop3.png" class="d-block w-100" width="800" height="400"alt="...">
					</div>
				</div>
				<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button>
				</div>
			</div>
		</div>

	</div>
</section>
<section class="py-0">
    <div class="container ">
        <div class="row text-center">
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                <div class="card bg-dark text-light mb-3 dd  ">
                    <img src="/images/h2.png" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title">Lorem.</h4>
                        <div class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure est dolorem facere dolor</div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                <div class="card bg-dark text-light mb-3 dd">
                    <img src="/images/h3.jpg" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title">Lorem.</h4>
                        <div class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure est dolorem facere dolor</div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                <div class="card bg-dark text-light mb-3 dd">
                    <img src="/images/h4.png" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title">Lorem.</h4>
                        <div class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure est dolorem facere dolor</div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                <div class="card bg-dark text-light mb-3 dd">
                    <img src="/images/h5.jpg" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title">Lorem.</h4>
                        <div class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure est dolorem facere dolor</div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                <div class="card bg-dark text-light mb-3 dd">
                    <img src="/images/wh.jpg" alt="" class="card-img-top">
                    <div class="card-body text-center">
                        <h4 class="card-title">Lorem.</h4>
                        <div class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure est dolorem facere dolor</div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>
<section class="py-5">
    <div class="container">
        <div class="row align-items-center justify-items-center">
            <div class="col-lg-4 ">
                <img src="/images/h2.png" alt="error" class="img-card">
            </div>
            <div class="col-lg-6 py-2">
                <h3>sdhfvu dsijgvuh</h3>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto, repellendus.</p>
            </div>
        </div>
    </div>
</section>
<script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

</body>
</html>



