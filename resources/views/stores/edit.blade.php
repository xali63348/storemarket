@extends('layouts.app')

@section('styles')
<style>
    <style>
    .title {
    font-size: 2.2rem;
    color: #444;
    margin-bottom: 10px;
    margin-left:90px;
    }

    .input-field {
    max-width: 380px;
    width: 100%;
    background-color: #f0f0f0;
    margin: 10px 0;
    height: 55px;
    border-radius: 55px;
    display: grid;
    grid-template-columns: 15% 85%;
    padding: 0 0.4rem;
    position: relative;
    }

    .input-field i {
    text-align: center;
    line-height: 55px;
    color: #acacac;
    transition: 0.5s;
    font-size: 1.1rem;
    }

    .input-field input {
    background: none;
    outline: none;
    border: none;
    line-height: 1;
    font-weight: 600;
    font-size: 1.1rem;
    color: #333;
    }

    .input-field input::placeholder {
    color: #aaa;
    font-weight: 500;
    }
    .btn1 {
  width: 150px;
  background-color: #5995fd;
  border: none;
  outline: none;
  height: 49px;
  border-radius: 49px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 600;
  margin: 10px 0;
  cursor: pointer;
  transition: 0.5s;
  margin-left:120px;
}

.btn1:hover {
  background-color: #4d84e2;
}

form{
    margin-left:10%;
}
input[type="file"]{
    display:none;
}
label{
    color:#fff;
     background-color:#4d84e2;
     position:absolute;
     height:40px;
     width:160px;
     justify-content:center;
     display:flex;
     border-radius: 4px;
}

.fa-images{
    margin-top:7px;
    margin-right:7px;
}

</style>
@endsection
@section('contant')
    <div class="container">
        <div class="row">
            <div class="col-lg-3 text-center">
                @component('shared.menu',['store'=>'active'])
                @endcomponent
            </div>
            <div class="col-lg-9">
            <form method='post' action="/stores/{{$store->id}}" class="sign-in-form" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <h2 class="title">Edit Store</h2>
                <div class="input-field">
                    <i class="fas fa-store-alt"></i>
                    <input type="text" name="name" id="name" value="{{$store->name}}" placeholder="Name" required/>
                </div>

                <div class="input-field">
                    <i class="fas fa-align-right"></i>
                    <input type="text" name="description" value="{{$store->description}}" id="description"  placeholder="Description" required />
                </div>
               <br>
                <div class="mb-3">
                    <input class="form-control" name="logo" type="file" id="file">
                    <label for="file">
                        <i class="fas fa-images"></i>
                        Choose Photo
                    </label>
                    
                </div>
                <br>
                <br>
                    <a href="/stores/{{$store->id}}/delete">delete store </a>
                    <br/>
                    <input type="submit" class="btn1" value="Edit" />
            </form>
            </div>
        </div>
    </div>
@endsection