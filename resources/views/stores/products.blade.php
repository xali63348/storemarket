
<style>
 .card img{
      height:250px;
      object-fit:cover;
}
.card{
    min-width: 250px;
    max-width: 250px;
     
}
.img-card{
	min-width: 300px;
    max-width: 300px;
}
.col-lg-4{
	text-align:center;
}
.dd:hover{
	box-shadow:3px 3px 14px 15px #ddd ;
}

 p{
  font-size: 12px;
}
  .aa{
    text-decoration: none;
    color:#fff;
}
.aa:hover{
    text-decoration: underline;
    color:#fff;
} 
.fa-plus{
    font-size:20px ;
}


.fas:hover{
    text-decoration: underline;
    color:black;
}
</style>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous">
    </script>
  
</head>
<body>
@include('shared.navbar')

<section class="py-0">
    <div class="container">
        <div class="row ">
            <div class="col-12">
                <a href="/products/create/{{$store->id}}"class="fas"><i class="fas fa-plus"> {{__('messages.Add product')}}</i></a>
            </div>
            <br>
            <br>
            @foreach($store->products as $product)
                <div class="col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-3">
                    <div class="card bg-primary text-light mb-3 dd text-center ">
                        @isset($product->images[0])
                        <a href="/products/{{$product->id}}/edit"><img src="{{asset($product->images[0])}}" class="card-img-top" alt="Error"></a>
                        @endisset
                        @empty($product->images[0])
                        <a href="/products/{{$product->id}}/edit"><img src="{{asset('/images/product.jpeg')}}" class="card-img-top" alt="Error"></a>
                        @endempty
                            <div class="card-body text-center">
                                <h4 class="card-title">{{ Illuminate\Support\Str::limit($product->name, 12,'...')}}</h4>
                                <a href="/products/{{$product->id}}/edit" class="aa"><p class="card-text">{{ Illuminate\Support\Str::limit($product->description, 50, '...') }}</p></a>
                               
                            </div>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</section>
@include('layouts.footer')
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

</body>
</html>

