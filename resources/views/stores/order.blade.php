@extends('layouts.app')

@section('styles')
    <style>
        table{
            text-align: center;
        }
        .id1{
            margin-right:20%;
           width: 20px;
        }
       
         
    </style>

@endsection


@section('contant')
<div class="container">
    <div class="row row text-center">
        <div class="col-12 col-sm-12 col-lg-3 ">
            @component('shared.menu',['StoreOrder'=>'active'])
            @endcomponent
        </div>
        <div class="table-responsive  col-lg-9 ">
         <table class="table">
            <thead>
                <tr>
                <th scope="col">{{__('messages.Id')}}</th>
                <th scope="col">{{__('messages.Stores')}}</th>
                <th scope="col">{{__('messages.Order number')}}</th>
                <th scope="col">{{__('messages.Payment')}}</th>
                <th scope="col">{{__('messages.Status')}}</th>
                <th scope="col">{{__('messages.Product')}}</th>
                <th scope="col">{{__('messages.Procedures')}}</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $order)
                <tr>
                <th scope="row">{{$order->id}}</th>

                <td>{{$order->store->name}}</td>
                <td>{{$order->transaction_id}}</td>
                <td>{{$order->payment_type}}</td>
                <td>{{$order->status}}</td>
                <td>{{sizeof($order->products)}}</td>
                <td>
                <div> <a href="/orders/{{$order->id}}/store" class="btn btn-outline-dark">{{__('messages.details')}}</a></div>
                </td>
                <td>
                @if(in_array($order->status,['paid','pending']))
                    <a href="/orders/{{$order->id}}/delivered" class="btn btn-primary">{{__('messages.Delivered')}}</a>
                @endif
                </td>
                </tr>
                @endforeach
            </tbody>
            </table>
           
        </div>
    </div>
</div>
@endsection