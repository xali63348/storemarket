@extends('layouts.app')
@section('styles')
<style>
.control[type="file"]{
    display:none;
}
.lab{
    color:#fff;
     background-color:#4d84e2;
     position:absolute;
     height:40px;
     width:160px;
     justify-content:center;
     display:flex;
     border-radius: 4px;
}

.fa-images{
    margin-top:7px;
    margin-right:7px;
}
.btn1 {
  width: 150px;
  background-color: #5995fd;
  border: none;
  outline: none;
  height: 49px;
  border-radius: 49px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 600;
  margin: 10px 0;
  cursor: pointer;
  transition: 0.5s;
  margin-left:120px;
}

.btn1:hover {
  background-color: #4d84e2;
}
.w-100{
    list-style:none;
}
.lii{
    text-align: center;
}

</style>
@endsection
@section('contant')

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="w-100 p-1 m-auto">@include('shared.message')</div>
        </div>
        <div class="col-sm-12  col-lg-6 col-md-7 m-auto">
            <form action="/users/support" method="post">
                @csrf  
                    <div class="mb-3 text-center">
                        <label for="exampleFormControlInput1" class="form-label "><h3>{{__('messages.Support')}}</h3></label>
                        <input type="email" class="form-control" name="email" id="email" value="{{auth()->user()->email}}" placeholder="{{__('messages.email')}}" >
                    </div>

                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label"></label>
                        <textarea class="form-control" name="text" id="text" placeholder="{{__('messages.What is the problem?')}}" rows="3" required></textarea>
                    </div>
                
                    <br>
                    <br>
                    <input type="submit" class="btn1" value="submit" />
                </form>
            </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
@endsection