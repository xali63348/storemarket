@extends('layouts.app')

@section('styles')
<style>
    .title {
    font-size: 2.2rem;
    color: #444;
    margin-bottom: 10px;
    margin-left:90px;
    }

    .input-field {
    max-width: 380px;
    width: 100%;
    background-color: #f0f0f0;
    margin: 10px 0;
    height: 55px;
    border-radius: 55px;
    display: grid;
    grid-template-columns: 15% 85%;
    padding: 0 0.4rem;
    position: relative;
    }

    .input-field i {
    text-align: center;
    line-height: 55px;
    color: #acacac;
    transition: 0.5s;
    font-size: 1.1rem;
    }

    .input-field input {
    background: none;
    outline: none;
    border: none;
    line-height: 1;
    font-weight: 600;
    font-size: 1.1rem;
    color: #333;
    }

    .input-field input::placeholder {
    color: #aaa;
    font-weight: 500;
    }
    .btn1 {
  width: 150px;
  background-color: #5995fd;
  border: none;
  outline: none;
  height: 49px;
  border-radius: 49px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 600;
  margin: 10px 0;
  cursor: pointer;
  transition: 0.5s;
  margin-left:120px;
}

.btn1:hover {
  background-color: #4d84e2;
}

form{
    margin-left:10%;
}
</style>
@endsection
@section('contant')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-3 text-center">
                @component('shared.menu',['address'=>'active'])
                @endcomponent
            </div>
            <div class="col-lg-9">
                @isset($address)
            <form method='post' action="/users/store-address" class="sign-in-form">
                @csrf
                <h2 class="title">{{__('messages.Edit address')}}</h2>
                <div class="input-field">
                    <i class="fas fa-globe-americas"></i>
                    <input type="text" name="Country" id="Country"  value="{{$address ['Country']}}" placeholder="{{__('messages.country')}}" required/>
                </div>

                <div class="input-field">
                    <i class="fas fa-city"></i>
                    <input type="text" name="area" id="area" value="{{$address ['area']}}" placeholder="{{__('messages.area')}}" required />
                </div>

                <div class="input-field">
                    <i class="fas fa-th"></i>
                    <input type="Number" name="block" id="block" value="{{$address ['block']}}" placeholder="{{__('messages.block')}}" required/>
                </div>

                <div class="input-field">
                    <i class="fas fa-street-view"></i>
                    <input type="Number" name="street" id="street" value="{{$address ['street']}}" placeholder="{{__('messages.street')}}" required/>
                </div>
                <div class="input-field">
                    <i class="fas fa-home"></i>
                    <input type="Number" name="house" id="house" value="{{$address ['house']}}" placeholder="{{__('messages.house')}}" required />
                </div>

                <div class="input-field">
                    <i class="fas fa-map-marked-alt"></i>
                    <input type="text" name="extra" id="extra" value="{{$address ['extra']}}" placeholder="{{__('messages.extra')}}"  />
                </div>
                    <input type="submit" class="btn1" value="{{__('messages.edit')}}" />
            </form>
            @endisset
            @empty($address)
            <form method='post' action="/users/store-address" class="sign-in-form">
                @csrf
                <h2 class="title">Edit address</h2>
                <div class="input-field">
                    <i class="fas fa-globe-americas"></i>
                    <input type="text" name="Country" id="Country"  placeholder="{{__('messages.country')}}" required/>
                </div>

                <div class="input-field">
                    <i class="fas fa-city"></i>
                    <input type="text" name="area" id="area"  placeholder="{{__('messages.area')}}" required />
                </div>

                <div class="input-field">
                    <i class="fas fa-th"></i>
                    <input type="Number" name="block" id="block" placeholder="{{__('messages.block')}}" required/>
                </div>

                <div class="input-field">
                    <i class="fas fa-street-view"></i>
                    <input type="Number" name="street" id="street"  placeholder="{{__('messages.street')}}" required/>
                </div>
                <div class="input-field">
                    <i class="fas fa-home"></i>
                    <input type="Number" name="house" id="house"  placeholder="{{__('messages.house')}}" required />
                </div>

                <div class="input-field">
                    <i class="fas fa-map-marked-alt"></i>
                    <input type="text" name="extra" id="extra"  placeholder="{{__('messages.extra')}}"  />
                </div>
                    <input type="submit" class="btn1" value="{{__('messages.edit')}}" />
            </form>
            @endempty
            </div>
        </div>
    </div>
@endsection