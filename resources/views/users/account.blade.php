@extends('layouts.app')
@section('styles')
    <style>
    .card{
      min-width: 250px;
      max-width: 250px;
      margin: 12px !important;
    }
    .card img{
      height:150px;
      object-fit:cover;
    }
    .yy{
        margin-left:30px;
    }
 
    </style>
@endsection
@section('contant')
<section>
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-sm-12 col-lg-3">
                @component('shared.menu',['store'=>'active'])
                @endcomponent
            </div>
                <div class="col-lg-9 ">
                    <div class="card-group ">
                        @foreach (auth()->user()->store as $store)
                        <div class="card mx-3 ">
                            @isset($store->images)
                                <img src="{{asset($store->images)}}" class="card-img-top" alt="Error">
                            @endisset
                            @empty($store->images)
                                <img src="{{asset('/images/store.jpeg')}}" class="card-img-top" alt="Error">
                            @endempty
                            <div class="card-body">
                                <h5 class="card-title">{{$store->name}}</h5>
                                <p class="card-text">{{$store->description}}</p>
                                <span><a href="/stores/{{$store->id}}/edit" class="btn btn-outline-dark">{{__('messages.edit')}}</a></span>
                                <span><a href="/stores/{{$store->id}}/products" class="btn btn-outline-dark yy">{{__('messages.edit product')}}</a></span>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div> 
        </div>
    </div>
</section>
@endsection
