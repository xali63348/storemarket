@extends('layouts.app')

@section('styles')
<style>
    .title {
    font-size: 2.2rem;
    color: #444;
    margin-bottom: 10px;
    margin-left:90px;
    }

    .input-field {
    max-width: 380px;
    width: 100%;
    background-color: #f0f0f0;
    margin: 10px 0;
    height: 55px;
    border-radius: 55px;
    display: grid;
    grid-template-columns: 15% 85%;
    padding: 0 0.4rem;
    position: relative;
    }

    .input-field i {
    text-align: center;
    line-height: 55px;
    color: #acacac;
    transition: 0.5s;
    font-size: 1.1rem;
    }

    .input-field input {
    background: none;
    outline: none;
    border: none;
    line-height: 1;
    font-weight: 600;
    font-size: 1.1rem;
    color: #333;
    }

    .input-field input::placeholder {
    color: #aaa;
    font-weight: 500;
    }
    .btn1 {
  width: 150px;
  background-color: #5995fd;
  border: none;
  outline: none;
  height: 49px;
  border-radius: 49px;
  color: #fff;
  text-transform: uppercase;
  font-weight: 600;
  margin: 10px 0;
  cursor: pointer;
  transition: 0.5s;
  margin-left:120px;
}

.btn1:hover {
    background-color: #4d84e2;
}

form{
    margin-left:10%;
}
.lii{
    list-style: none;
}
</style>
@endsection
@section('contant')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-3 text-center">
                @component('shared.menu',['information'=>'active'])
                @endcomponent
            </div>
            <div class="col-lg-9">
            <form method='post' action="/users/edit-email" class="sign-in-form">
                @csrf
                @method('PUT')
                <div class="w-50 p-1">@include('shared.error')</div>
                <h2 class="title">{{__('messages.edit email')}}</h2>
                <div class="input-field">
                <i class="fas fa-envelope"></i>
                    <input type="text" name="email" id="email" value="{{$user->email}}" placeholder="{{__('messages.email')}}" required/>
                </div>

                <div class="input-field">
                    <i class="fas fa-lock"></i>
                    <input type="password" name="password" id="password" placeholder="{{__('messages.password')}}" required/>
                </div>
                <div><p>{{__('messages.Please enter the password to change the email')}}</p></div>

                    <input type="submit" class="btn1" value="{{__('messages.edit')}}" />
            </form>
            </div>
        </div>
    </div>
@endsection