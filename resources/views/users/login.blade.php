<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous"
    ></script>
    
    <link rel="stylesheet" href=" {{ URL::asset('register.css') }}">
</head>
<body>
    
<div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          <form method='post' action="/users/login" class="sign-in-form">
            @csrf
            @include('shared.error')
            <h2 class="title">{{__('messages.login')}}</h2>
            <div class="input-field">
              <i class="fas fa-envelope"></i>
              <input type="email" name="email" id="email" placeholder="{{__('messages.email')}}" />
            </div>
            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="password" id="password" placeholder="{{__('messages.password')}}" />
            </div>
            <input type="submit" class="btn" value="{{__('messages.login')}}" />
          </form>

        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
           
            <p>
              {{__('messages.Login and browse all products and shop')}}
            </p>
             <a href="/users/store" class="btn btn-outline-primary">{{__('messages.Sign up')}}</a>
          </div>
         
          <img src="{{asset('images/login.svg')}}" class="image" alt="" />
        </div>
    </div>

    


    <script src="login.js"></script>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
</body>
</html>


















