<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous"
    ></script>
    
    <link rel="stylesheet" href=" {{ URL::asset('register.css') }}">
</head>
<body>
    
<div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          <form method='post' action="/users/store" class="sign-in-form">
            @csrf
            @include('shared.error')
            <h2 class="title">{{__('messages.Sign up')}}</h2>
            <div class="input-field">
              <i class="fas fa-user"></i>
              <input type="text" name="name" id="name"  placeholder="{{__('messages.name')}}" required/>
            </div>

            <div class="input-field">
              <i class="fas fa-envelope"></i>
              <input type="email" name="email" id="email" placeholder="{{__('messages.email')}}" required />
            </div>

            <div class="input-field">
            <i class="fas fa-mobile-alt"></i>
              <input type="Number" name="number" id="number" placeholder="{{__('messages.number phone')}}" required/>
            </div>

            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="password" id="password" placeholder="{{__('messages.password')}}" required/>
            </div>
            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="confirm" id="confirm" placeholder="{{__('messages.Confirm Password')}}" required />
            </div>
            <input type="submit" class="btn" value="{{__('messages.Sign up')}}" />
          </form>

        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
           
            <p>
              {{__('messages.sing')}}
            </p>
             <a href="/users/login" class="btn btn-outline-primary">{{__('messages.login')}}</a>
          </div>
         
          <img src="{{asset('images/sing_in.svg')}}" class="image" alt="" />
        </div>
    </div>

    


    <script src="login.js"></script>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
</body>
</html>


















