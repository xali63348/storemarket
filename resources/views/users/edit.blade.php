@extends('layouts.app')

@section('styles')
<style>

a{
    text-decoration:none;
}
a:hover{
    text-decoration:underline;
    color:black;
}
.tr:hover{
    background:#E5E5E5;
}

</style>
@endsection
@section('contant')
    <div class="container">
        <div class="row text-center">
            <div class="col-12 col-sm-12 col-lg-3">
                    @component('shared.menu',['information'=>'active'])
                    @endcomponent
            </div>
            <div class="table-responsive col-lg-9">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">{{__('messages.Edit Information')}}</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr">
                    <th class="w-25" scope="row">{{__('messages.name')}}</th>
                    <td class="w-25">{{$user->name}}</td>
                    
                    <td class="w-25"><a href="/users/edit-name">{{__('messages.edit')}}</a></td>
                    </tr>
                    <tr class="tr">
                    <th class="w-25" scope="row">{{__('messages.number phone')}}</th>
                    <td class="w-25">{{$user->number}}</td>
                    
                    <td class="w-25"><a href="edit-number">{{__('messages.edit')}}</a></td>
                    </tr>
                    <tr class="tr">
                    <th class="w-25" scope="row">{{__('messages.email')}}</th>
                    <td class="w-25">{{$user->email}}</td>
                    
                       <td class="w-25"><a href="/users/edit-email">{{__('messages.edit')}}</a></td>
                    </tr>
                    <tr class="tr">
                    <th class="w-25" scope="row">{{__('messages.password')}}</th>
                    <td class="w-25">**********</td>
                    
                     <td class="w-25"><a href="/users/edit-password">{{__('messages.edit')}}</a></td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>

@endsection
