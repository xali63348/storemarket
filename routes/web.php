<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('toArabic','SessionsController@toArabic');
Route::get('toEnglish','SessionsController@toEnglish');
Route::get('/','HomeController@home');

Route::get('/test2',function(){
    return view('test2');
});
Route::get('/search','ProductController@search');


// Route::get('/execute-payment','PaymentController@execute');
Route::post('/create-payment','PaymentController@create');

Route::any('payment/paypal/callback','PaymentController@callback')->name('paypal.callback');
Route::any('payment/paypal/cancel','PaymentController@cancel')->name('paypal.cancel');


Route::prefix('users')->group(function(){
    Route::get('/login','UserController@login')->name('login');
    Route::post('/login','UserController@logintry');
    Route::get('/store','UserController@store');
    Route::post('/store','UserController@create');

    Route::group(['middleware'=>['auth']],function(){
        Route::get('/account','UserController@account');
        Route::get('/logout','UserController@logout');
        Route::get('/address','UserController@address');
        Route::post('/store-address','UserController@StoreAddress');
        Route::get('/edit','UserController@edit');

        Route::get('/edit-name','UserController@EditName');
        Route::put('/edit-name','UserController@NameUpdate');

        Route::get('/edit-email','UserController@EditEmail');
        Route::put('/edit-email','UserController@EmailUpdate');

        Route::get('/edit-number','UserController@EditNumber');
        Route::put('/edit-number','UserController@NumberUpdate');

        Route::get('/edit-password','UserController@EditPassword');
        Route::put('/edit-password','UserController@PasswordUpdate');

        Route::get('/support','UserController@Support');
        Route::post('/support','UserController@SendSupport');
    });
    
});
    Route::prefix('stores')->group(function(){

        Route::group(['middleware'=>['auth']],function(){
            Route::get('/create','StoreController@create');
            Route::post('/store','StoreController@store');
            Route::get('/orders','StoreController@orders');
            Route::get('/{store}/edit','StoreController@edit');
            Route::put('/{store}','StoreController@update');
            Route::get('/{store}/delete','StoreController@destroy');
            Route::get('/{store}/products','StoreController@products');
            

        });
        
    });
    Route::prefix('products')->group(function(){
        Route::group(['middleware'=>['auth']],function(){
        Route::get('/create/{store}','ProductController@create');
        Route::post('/{store}','ProductController@store');
        Route::get('/{product}/delete','ProductController@destroy');
        Route::get('/{product}/edit','ProductController@edit');
        Route::put('/{product}','ProductController@update');
        });
        Route::get('/{product}','ProductController@show');
        
    });

    Route::prefix('orders')->group(function(){
        Route::group(['middleware'=>['auth']],function(){
        Route::get('/create','OrderController@create');
        Route::get('/buy','OrderController@buy');
        Route::post('/addProduct/{product}','OrderController@addProduct');
        Route::get('/payment','OrderController@payment');
        Route::get('/delete','OrderController@destroy');
        Route::get('/order','OrderController@index');
        Route::get('/{order}','OrderController@show');
        Route::get('/{order}/delivered','OrderController@Delivered');
        Route::get('/{order}/store','OrderController@store');
        });
        
    });
   