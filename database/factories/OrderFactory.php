<?php

namespace Database\Factories;


use App\Models\Order;
use App\Models\Product;
use App\Models\store;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    function generateProducts(){ 
        $products=array();
        for($i=0; $i<rand(1,3);$i++){ 
            $product=Product::all()->random();
            $product->quantity=rand(1,4);
            $products[]=$product;
        }
        return ($products);
    }
    public function definition()
    {
        return [
            'transaction_id' => $this->faker->randomNumber($nbDigits = 8, $strict = false),
            'payment_type'=>$this->faker->randomElement($array=array('Knet','cash','paypal')),
            'status'=>$this->faker->randomElement($array=array('pending','paid','delivered','failed')),
            'products'=>$this->generateProducts(),
            'store_id'=> Store::all()->random()->id,
            'user_id'=> User::all()->random()->id
        ];
    }
}
