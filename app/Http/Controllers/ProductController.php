<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Store $store) //دالة لعرض صفحة انشاء سلعه
    {
        if($store->user_id != auth()->user()->id){ //التاكد من اي دي اليوزر المسجل لدخول يكون نفس اي دي المتجر لكي لا يكون لايمكن لتاجر التنقل بين الصفحات بقية التجار عن طريق اي دي
            return view('message Error.message-store',['message'=>__('messages.Sorry, this page is not available')]);
        }
        return view('product.create',compact('store'));
    }

    public function search(Request $request){ //دالة لبحث عن السلع
        $query=$request->input('query'); //هنا نعطي متغير "كويري" اسم المنتج الذي قام اليوزر بالبحث عنه
        if(empty($query)){ //اذا لم يكن هنالك اي عملية بحث نعطي مصفوفه فارغه
            $data=[];
        }elseif(!empty($query)){ //اذا قام المستخدم بالبحث عن المنتج و هوه يوجد في متجر يتم اضهاره بارسال "داته" الى واجهه
            $data= Product::where('name','like','%'.$query.'%')->get();
            if(empty($data)){//اذا ادخل مستخدم منتج غير موجود في متجر سوف يضهر له رساله انه لا يوجد هذا العنصر
                $result='no result';
                return view('search',compact('result'));

            }
        }
        return view('search',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,Store $store) //دالة لاضافة السلع في الداته بيس 
    {
        if($store->user_id != auth()->user()->id){ //التاكد من اي دي اليوزر المسجل لدخول يكون نفس اي دي المتجر لكي لا يكون لايمكن لتاجر التنقل بين الصفحات بقية التجار عن طريق اي دي
            return view('message Error.message-store',['message'=>__('messages.Sorry, this page is not available')]);
        }

        $validatedDate=request()->validate([
            'name'=>'required|min:3',
            'description'=>'max:100',
            'price'=>'required|min:1',
            'images.*'=>'mimes:bmp,webp,png,jpg,jpeg|max:10000',
           
        ]);
        $path=array(); //اعطاء باث مصفوفه فارغه في حال لم يرفع المستخدم صوره
        if($request->hasFile('images')){ //هنا نقوم بعمل شرط اذا قام اليوزر برفع صوره 
            foreach ($request->file('images') as $file){
                $path[]='/storage/'.$file->store('logos',['disk'=>'public']);//عمل مصفوفه وضع فيها جمع الصور التي تم ارفقها من المستخم
            }
        }
        $newproduct= new Product();
        $newproduct->name=$request->name;
        $newproduct->description=$request->description;
        $newproduct->price=$request->price;
        $newproduct->images=$path;
        $store->products()->save($newproduct); //نقوم بحفظ السلعه الجديده بهذه الطريقة لكي نقوم بربط بين السلعه و المتجر بنفس الوقت
        return redirect('/stores/'.$store->id.'/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
       return view('product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product) //دالة لتعديل على السلع 
    { 
        if($product->store->user_id != auth()->user()->id){
            return view('message Error.message-store',['message'=>__('messages.Sorry, this page is not available')]);
        }
        return view('product.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product) //في هذه الدالة يقوم التاجر بتعديل معلومات السلع التي قام بانشائها سابقا
    {
        if($product->store->user_id != auth()->user()->id){ //شرط لتاكد من اليوزر اي دي "لمستخدم" اذا كان لا يساوي اي دي اليوزر اظهار رسالة ان الصفحة غير متوفره 
            return view('message Error.message-store',['message'=>__('messages.Sorry, this page is not available')]);
        }

        $validatedDate=request()->validate([ 
            'name'=>'required|min:3',
            'description'=>'required|max:100',
            'price'=>'required|min:1',
            'images.*'=>'mimes:bmp,webp,png,jpg,jpeg|max:100000'
        ]);
        $path=$product->images; //هنا اقوم بأعطاء "الباث"الصور القديمه في حال قام التاجر بتغير فقط معلومات السلعة وليس الصور

        if($request->hasFile('images')){//هنا شرط اتاكد من اذا قام التاجر بارفاق صور لسلع
            $path= array(); //اعطي مصفوفه فارغه لكي يقوم برفع الصور الجديده
            foreach($request->file('images') as $file){ //هنا عملية اضافة اكثر من صوره 
                $path[]='/storage/'.$file->store('logos',['disk'=>'public']);
            }
        }
        $product->name=$request->name;
        $product->description=$request->description;
        $product->price=$request->price;
        $product->images=$path;
        $product->save();
        return redirect('/stores/'.$product->store->id.'/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product) //دالة لحذف السلعه
    {
        if($product->store->user_id != auth()->user()->id){
            return view('message Error.message-store',['message'=>__('messages.Sorry, this page is not available')]);
        }
        $product->delete();
        return redirect('/stores/'.$product->store->id.'/products');
    }
}
