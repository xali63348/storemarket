<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() //دالة لعرض صفحة انشاء المتاجر
    {
        return view('stores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//دالة لاضافة بيانات المتجر في الداته 
    {
        $validatedDate=request()->validate([
            'name'=>'required|min:2',
            'description'=>'required|min:5',
            'logo'=>'mimes:jpg,png,jpeg,bmp|max:3000'
        ]);
        
        $path=null; //اعطاء قيمه فارغه لمسار باث في حال لم يقم المستخدم في رفع الصور
        if($request->hasFile('logo')){ //التاكد من وجود صوره هل قام المستخدم برفع صوره او لا
            $path = '/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);//لانضيف الصور بشكل موباشر الى داته بيس بل نضعها في مسار مثل باث
        }

        $newStore=new Store(); //انشاء متجر جديد
        $newStore->name=request()->name;
        $newStore->description=request()->description;
        $newStore->images=$path;
        auth()->user()->store()->save($newStore);  //حفظ بيانات المتجر الجديد عن طريق الوصول الى ستور بواسطة اوث نستعمل هذه الطريقه لكي نربط المتجر مع المستخدم لاننا عرفنه علاقه بين المتجر و المستخدم في المودل
        return redirect('/users/account');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store) //داله لعرض صفحة تعديل على المتجر
    {
        if($store->user_id != auth()->user()->id){ //شرط لتحقق مناذا كان  يوزر اي دي لا يساوي اي دي المستخدم المسجل دخول
            return view('message Error.message-store',['message'=>'Sorry, this page is not available']);//اضهار صفحه هذه الصفحه غير متوفره
        }
        return view('stores.edit',compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store) //داله لتعديل على المتجر من اسم و وصف و صوره
    {
        $validatedDate=request()->validate([
            'name'=>'required|min:2',
            'description'=>'required|min:5',
            'logo'=>'mimes:jpg,png,jpeg,bmp|max:3000'
        ]);
        $path=$store->images; //اعطاء مسار باث الصوره الحاليه لمتجر في حال لم يرفع صوره جديده

        if($request->hasFile('logo')){ //التاكد من وجود صوره هل قام المستخدم برفع صوره او لا
            $path = '/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);//لانضيف الصور بشكل موباشر الى داته بيس بل نضعها في مسار مثل باث
        }

        $store->name=request()->name;
        $store->description=request()->description;
        $store->images=$path;
        $store->save();
        return redirect('/users/account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store) //داله لحذف المتجر
    {
        if($store->user_id != auth()->user()->id){ //شرط لتحقق مناذا كان  يوزر اي دي لا يساوي اي دي المستخدم المسجل دخول
            return view('message Error.message-store',['message'=>'Sorry, this page is not available']);//اضهار صفحه هذه الصفحه غير متوفره
        }
        $store->delete();
        return redirect('/users/account');
    }
    public function products(Request $request, Store $store){ //دالة لعرض السلع و التعديل عليها و اضافة سلع
        if($store->user_id != auth()->user()->id){
            return view('message Error.message-store',['message'=>'Sorry, this page is not available']);
        }
       return view('stores.products',compact('store'));
    }
    public function orders(){

        $stores=auth()->user()->store; //انشئنا متغير نضع فيه جميع متاجر خاصه في المستخدم المسجل دخولة
        $orders=array(); //مصفوفه فارغه
        foreach($stores as $store){
           foreach ($store->orders as $order) { //هنا وصلنا لجميع طلبات الخاصه في المتجر لمستخدم المسجل دخولة وضعناه في مصفوفه ارسلناه الى واجهه
            $orders[]=$order;
           }
        }
        return view('stores.order',compact('orders'));
    }
}
