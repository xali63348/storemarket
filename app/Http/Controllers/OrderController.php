<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\store;
use App\Mail\OrderReceived;
use App\Mail\OrderInvoiceReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() //دالة لعرض صفحة طلبات الزبون و روية حالت الطلب و تفاصيل
    {
        $orders=auth()->user()->orders;
        return view('order.index',compact('orders'));
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order) //دالة لعرض السلة لمتسخدم و عدد مشتريات
    {
        if(!session()->has('carruntOreder')){
            return redirect('/');
        }
        $total=0;
        foreach(session('carruntOreder') as $product){
           $total+= $product->price*$product->quantity;//حساب مجموع كلي لطلبات زبون
        }
        return view('order.create',compact('total'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addProduct(Product $product){
        $validatedData=request()->validate([
            'quantity'=>'required|min:1'
        ]);
        //هنا انشئنا متغير و في داخله قلنا اذا يوجد متغير باسم "كرنت اوردر" اعطي متغير في الجلسه الى متغير $ اذا لم يكن هنالك متغير في الجلسه اعطني مصفوفه فارغه
        $carruntOreder=session()->has('carruntOreder') ? session('carruntOreder'):array();
        if(!empty($carruntOreder)){  // ملخص هذا الشرط اذا كان "كرنت اوردر" غير فارغ 
            //شرط اخر اذا كان ستور اي دي ل "كرنت اوردر لا يساوي يوزر اي دي لبرودكت اعطي "كرنت اوردر" مصفوفه فارغه اي تفريغ جمع السلع التي تم التسوق منها من متجر اخر
            if($carruntOreder[0]->store_id != $product->store_id)
                $carruntOreder=array();
        }
        $alrady=false;  //اعطاء متغير "ال ريدي" قيمه فولس
        foreach($carruntOreder as $order){   //عبارة دوران في داخلها شرط
            if($order->id == $product->id){ //هنا شرط اذا كان اي دي "اوردر" يساوي اي دي "برودكت" اعطي "ال ريدي" ترو
                $alrady=true;
                //هنا قمنا بجمع الكميه القديمه مع الكمية الجديده اذا كان اليوزر متسوق من نفس المتجر
                $order->quantity +=request()->quantity;
            }
        }
        //اذا كان "ال ريدي" فولس
        if(!$alrady){
            //اعطاء كمية اشياء تسوقها اليوزر الى متغير "برودكت" ا
            $product->quantity=request()->quantity;
            $carruntOreder[]=$product;
        }
        session(['carruntOreder'=>$carruntOreder]);
        return back()->with('message',[__('messages.The request has been added successfully')]);
    
    }
    public function store(Order $order) //دالة لعرض تفاصيل طلب الزبون و معلوماته و عنوانه
    {
        foreach (auth()->user()->store as $stores ) {
            if($order->store_id == $stores->id){ //شرط اذا كان "ستور اي دي" يساوي "ستور اي دي لمتجر"سوف تضهر الصفحه لتاجر هذا الشرط وضع لكي لايقدر اي تاجر على التنقل بين طلبات المتاجر عن طريق اي دي
            $total=0;
            foreach($order->products as $product){
                $total+=$product['price']*$product['quantity']; //حساب مجموع كلي لطلبات زبون
            }
            return view('order.store',compact('order','total'));
        }
    }
    return back();
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        if($order->user_id == auth()->user()->id){ //شرط لتاكد من ان اليوزر اي دي ل "اوردر" يساوي يوزر اي دي لمستخدم المسجل دخولة
            $total=0;
            foreach($order->products as $product){ //جملة دوران لحساب قيمة الكلية لمشتريات لمستخدم
                $total+=$product['price']*$product['quantity'];
            }
            return view('order.show',compact('order','total'));
        }
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)//دالة لحذف السلة 
    {
        session()->forget('carruntOreder');
        return redirect('/');
    }
    public function payment(){
        // في هذا شرط قلت اذا يوجد متغير "كرنت اوردر" في الجلسه
        // اعرض عليه حالة الطلب ايضا تفاديا لمشكلة عندما يقوم اليوزر
        // بأعادة تحميل الصقحة يظهر له مشكلة لانه بأعادة تحميل الصفحة يقوم بدفع مرة اخرى هذا خطا
        if(!empty(session('carruntOreder'))){ 
            $neworder=new Order();
            $neworder->transaction_id=rand(10,1000000000);
            $neworder->payment_type='cash';
            $neworder->status='pending';
            $products=session('carruntOreder');
            $neworder->user_id=auth()->user()->id;
            $neworder->store_id=$products[0]->store_id;
            $neworder->products=$products;
            $neworder->save();
            session()->forget('carruntOreder');
            
            Mail::to($neworder->store->user)->send(new OrderReceived($neworder));//ارسال ايميل لتنبيه صاحب المتجر بأن هناك طلب جديد من متجرك
            Mail::to($neworder->user->email)->send(new OrderInvoiceReceived($neworder));//ارسال ايميل لزبون الذي تسوق من متجر و ارفاق رابط لروية تفاصيل طلبه
            return view('order.payment');
        }
        return redirect('/');
    }
    public function Delivered(Order $order){ //دالة توصيل طلبات
        if($order->store->user_id != auth()->user()->id){ //في هذا الشرط اتاكد من ان يوزر اي دي "لمتجر" اذا لايساوي اي دي لمستخدم المسجل دخولة اعرض عليه رسالة ان الصفحة غير متوفره
            return view('message Error.message-store-order',['message'=>__('messages.Sorry, you do not have these privileges')]);
        }
        if(!in_array($order->status,['paid','pending'])){ //في هذا شرط دالة لتحقق من ان اذا كان لايوجد في حالة الطلب احد هاذين المتغيرين في المصفوفه تعرض عليه ان صفحه غير متوفره 
            return view('message Error.message-store-order',['message'=>__('messages.Sorry, you do not have these privileges')]);
        }
        $order->status='delivered';
        $order->save();
        return back();
    }
   
    
   
}
