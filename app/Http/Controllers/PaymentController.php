<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use App\Models\Order;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalHttp\HttpException;

class PaymentController extends Controller
{
    
    protected function getClient(){ //في هذه الداله خليت معلومات البايبال من كلاينت و سيكرت لكي استعملها اكثر من مكان الدالة "برودكت"
        $config=config('services.paypal');//هنا كلاينت اي دي فيه معلومات "الكونفك" وايضا "سيكرت اي دي" و ايضا "كلاينت اي دي " 
        $environment = new SandboxEnvironment($config['client_id'],$config['client_secret']);
        $client = new PayPalHttpClient($environment);
        return $client;
    }
    protected function getTotal(){ //هنا دالة فيها معلومات طلب الزبون من مجموع كلي لحساب و تفاصيل المنتج الذي تم طلبه
        $total=0;
       foreach(session('carruntOreder') as $product){
          $total+= $product->price*$product->quantity;//حساب مجموع كلي لطلبات زبون
       }
       return $total;
    }
    public function create(){ //في هذه الدالة انشاء طلب اوردر ارسالة الى بايبال مع بعض معلومات
       $client=$this->getClient();
       $total=$this->getTotal();

        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
                            "intent" => "CAPTURE",
                            "purchase_units" => [[
                                "reference_id" => "1242",
                                "amount" => [ //السعر الكلي المراد خصمه من بابيال و نوع العملة
                                    "value" => $total,
                                    "currency_code" => "USD"
                                ]
                            ]],
                            "application_context" => [ //روابط في حالة رفض العملة او نجاح اعادة توجيه الزبون
                                "cancel_url" => url(route('paypal.cancel')),
                                "return_url" => url(route('paypal.callback')),
                            ] 
                        ];
    
        try { //في حال كان الطلب ناجح خصم المبلغ و ايضا توجيه الزبون
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            if( $response->statusCode == 201 && isset($response->result) ){
                Session::put('paypal_order_id',$response->result->id);
            foreach($response->result->links as $link){
                if($link->rel == 'approve'){
                    return redirect()->away($link->href);
                }
            }
        }
            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            print_r($response);
        }catch (HttpException $ex) { //في حالة فشل او تراجع الزبون عن الطلب اضهارة رسالة تنبيهيه بفشل الطلب
            echo $ex->statusCode;
            print_r($ex->getMessage());
        }
    }
   
    public function callback(Request $request){ //هذه الدلة يتوجه اليها المستخدم في حالة نجاح العملية تكون فيها معلومات التي ارسلها بايبال
        $order_id=Session::get('paypal_order_id');
        $request = new OrdersCaptureRequest($order_id);
        $request->prefer('return=representation');
        try {
            $client=$this->getClient();
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
         
            if( $response->statusCode == 201 && isset($response->result) ){ //في هذا الشرط اذا كان الطلب ناجح حفظ بيانات الطلب في داته بيس
                if($response->result->status == 'COMPLETED'){
                    $newOrder=new Order();
                    $newOrder->transaction_id=$response->result->id;
                    $newOrder->payment_type="PayPal";
                    $newOrder->status="paid";
                    $products=session('carruntOreder');
                    $newOrder->products=$products;
                    $newOrder->user_id=auth()->user()->id;
                    $newOrder->store_id=$products[0]->store_id;
                    $newOrder->save();
                    session()->forget('carruntOreder');
                    Session::forget('paypal_order_id');
                    return view('/order/payment');
                }
             }
            // If call returns body in response, you can get the deserialized version from the result attribute of the response
            print_r($response);
        }catch (HttpException $ex) { //في حالة فشل او تراجع الزبون عن الطلب اضهارة رسالة تنبيهيه بفشل الطلب
            echo $ex->statusCode;
            print_r($ex->getMessage());
        }
    }
    public function cancel(){
        session()->forget('carruntOreder');
        return redirect("/");
    }
}
