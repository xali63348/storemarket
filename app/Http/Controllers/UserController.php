<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Store;
use App\Mail\UserReceived;
use App\Mail\UserAddressReceived;
use App\Mail\SupportInvoiceReceived;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Hash;‏
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function login(){ //دالة التحقق من وجود جلسه او لا
        if(auth()->check()){
           return redirect('/');
        }
        return view('users.login'); //اذا لم يوجد جلسه اضهار صفحة تسجيل الدخول
    }
    public function logout(){ //دالة لمسح الجلسة
        auth()->logout();
        session()->flush();
        return redirect('/');
    }

    public function logintry(){ //دالة لمحاولة تسجيل الدخول 
        $validatedData=request()->validate([
            'email'=>'required',
            'password'=>'required|min:6'
        ]);
        if(auth()->attempt(['email'=>request()->email, 'password'=>request()->password])){
            return redirect()->intended('/');
        }else{
            return back()->withErrors(['معلومات خاطئه']);
        }
    }
    public function store(){ //دالة لعرض صفحة الستجيل
        return view('users.register');
    }
    public function create(){ //دالة لانشاء حساب جديد
        $validatedData=request()->validate([
            'name'=>'required|min:3',
            'email'=>'required|unique:users',
            'number'=>'required|min:8|unique:users',
            'password'=>'required|min:6',
            'confirm'=>'required|same:password'
        ]);
        $newUser= new User();
        $newUser->name=request()->name;
        $newUser->email=request()->email;
        $newUser->number=request()->number;
        $newUser->password=bcrypt(request()->password);
        $newUser->email_verified_at=now();
        $newUser->save();
        Mail::to($newUser->email)->send(new UserReceived($newUser)); //هنا اقوم بارسال رسالة ترحب في الايميل لمستخدم الجديد
        if(empty($newUser->address)){ //في هذا الشرط اقول اذا لايوجد عنوان لمستخدم
            Mail::to($newUser->email)->send(new UserAddressReceived($newUser));//اقوم بارسال رسالة لمستخدم الجديد انبهه بانه يجيب وضع عنوانه الكامل ليتمكن طلب السلع من المتجر
        }
        if(auth()->attempt(['email'=>request()->email,'password'=>request()->password])){ //التاكد من البريد و الباسورد لتحويل الى صفحة الرئسيه
            return redirect('/');
        }
        return redirect('/');
    }
    public function account(){ //دالة لعرض صفحة اليوزر و عرض متاجره
        return view('users.account');
    }
    public function address(){ //دالة لعرض صفحة اضافة العنوان
        $address=auth()->user()->address;
        return view('users.address',compact('address'));
    }
    public function StoreAddress(){ //دالة لاضافة معلومات العنوان الى داته بيس
        $validatedData =request()->validate([
            'Country'=>'required',
            'area'=>'required',
            'block'=>'required',
            'street'=>'required',
            'house'=>'required'
        ]);
        $newaddress=[ //وضع العنوان في مصفوفه و اضافة كود في المودل لأبلاغ الداته بأن الادريس عبارة عن مصفوفه
            'Country'=>request()->Country,
            'area'=>request()->area,
            'block'=>request()->block,
            'street'=>request()->street,
            'house'=>request()->house,
            'extra'=>request()->extra,
        ];
        auth()->user()->address=$newaddress;
        auth()->user()->save();
        return redirect('/users/address');
    }
    public function edit(){ //دالة لعرض صفحة تعديل معلومات المستخدم
        $user=auth()->user();
        return view('users.edit',compact('user'));
    }
    public function EditName(){//دالة لعرض صفحة تعديل اسم المستخدم
        $user=auth()->user();
        return view('users.Edit-Name',compact('user'));
    }
    public function NameUpdate(){ //دالة لتحديث اسم المستخدم مع التاكد من باسورد الحساب
        $validatedData=request()->validate([
            'name'=> 'required|min:3',
            'password'=>'required|min:6'
        ]);
        
        if(Hash::check(request()->get('password'),auth()->user()->password)){//دالة لتحقق من باسورد الحساب اذا كان مطابق لما ادخلة المستخدم من باسورد وبعده يتم تحديث الاسم
            $user=auth()->user();
            $user->name=request()->name;
            $user->save();
            return redirect('/users/edit');
        }
        else{//اذا ادخل باسورد خطا يظهر له رسالة خطا
            return back()->withErrors(['معلومات خاطئه']);
        }
    }
    public function EditEmail(){//دالة لعرض صفحة تعديل الايميل
        $user=auth()->user();
        return view('users.Edit-Email',compact('user'));
    }
    public function EmailUpdate(){//دالة لتحديث ايميل المستخدم مع التاكد من باسورد الحساب
        $validatedData=request()->validate([
            'email'=>'required|unique:users,id,'. auth()->user()->id,
            'password'=>'required|min:6'
        ]);
        if(Hash::check(request()->get('password'),auth()->user()->password)){//دالة لتحقق من باسورد الحساب اذا كان مطابق لما ادخلة المستخدم من باسورد وبعده يتم تحديث الايميل
            $user=auth()->user();
            $user->email=request()->email;
            $user->email_verified_at=now();
            $user->save();
            return redirect('/users/edit');
        }
        else{//اذا ادخل باسورد خطا يظهر له رسالة خطا
            return back()->withErrors(['معلومات خاطئه']);
        }
    }
    public function EditNumber(){//دالة لعرض صفحة تعديل الرقم
        $user=auth()->user();
        return view('users.Edit-Number',compact('user'));
    }
    public function NumberUpdate(){//دالة لتحديث رقم المستخدم مع التاكد من باسورد الحساب
        $validatedData=request()->validate([
            'number'=>'required|min:8|unique:users',
            'password'=>'required|min:6'
        ]);
        if(Hash::check(request()->get('password'),auth()->user()->password)){//دالة لتحقق من باسورد الحساب اذا كان مطابق لما ادخلة المستخدم من باسورد وبعده يتم تحديث الرقم
            $user=auth()->user();
            $user->number=request()->number;
            $user->save();
            return redirect('/users/edit');
        }
        else{ //اذا ادخل باسورد خطا يظهر له رسالة خطا
            return back()->withErrors(['معلومات خاطئه']);
        }
    }
    public function EditPassword(){ //دالة لعرض صفحة تعديل الباسورد
        return view('users.Edit-Password');
    }
    public function PasswordUpdate(){//دالة لتحديث باسورد المستخدم مع التاكد من باسورد الحساب
        $validatedData=request()->validate([
            'password'=>'required|min:6',
            'newpassword'=>'required|min:6',
            'confirm'=>'required|same:newpassword'
        ]);
        if(Hash::check(request()->get('password'),auth()->user()->password)){//دالة لتحقق من باسورد القديم الحساب اذا كان مطابق لما ادخلة المستخدم من باسورد وبعده يتم تحديث الباسورد اضافة باسورد جديد
            $user=auth()->user();
            $user->password=bcrypt(request()->newpassword);
            $user->save();
            return redirect('/users/edit');
        }
        else{ //اذا ادخل باسورد خطا يظهر له رسالة خطا
            return back()->withErrors(['معلومات خاطئه']);
        }
    }
    public function Support(){
        return view('users.support');
    }
    public function SendSupport(){
        $validatedData=request()->validate([
            'email'=>'required',
            'text'=>'required|min:3'
        ]);
        $submit=request()->text;
        Mail::to('Zulekha@mail.com')->send(new SupportInvoiceReceived($submit));
        return back()->with('message',[__('messages.support')]);
    }
}
